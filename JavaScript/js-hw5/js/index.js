'use strict';

function createNewUser() {
    this.userFirstName = prompt("Enter your first name", "Rick");
    this.userLastName = prompt("Enter your last name", "Willson");
    this.userAge = prompt("Your Birthday `dd.mm.yyyy`", "27.08.1991");
    this.getLogin = function () {
        this.simpleName = this.userFirstName.charAt(0).toLowerCase() + this.userLastName.toLowerCase();
        return this.simpleName
    }
    this.getAge = function () {
        let dateNow = new Date();
        let birthdayDate = Date.parse(this.userAge.split(".").reverse().join("."));
        this.age = Math.floor((dateNow - birthdayDate) / (1000 * 60 * 60 * 24 * 365));
        return this.age;
    }
    this.getPassword = function () {
        this.password = this.userFirstName.charAt(0).toUpperCase() + this.userLastName.toLowerCase() + this.userAge.split("." )[2];
        return this.password;
    }
}
let newUser = new createNewUser();
console.log(newUser.getLogin());
console.log(newUser.getAge());
console.log(newUser.getPassword());
console.log('Can\'t say this')