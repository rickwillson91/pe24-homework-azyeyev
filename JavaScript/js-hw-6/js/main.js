"use strict";

function filterBy(list, type) {
    let userList = [];
    for (let i = 0; i < list.length; i++) {
        if(typeof(list[i]) !== type) {
            userList.push(list[i]);
        }
    }
    console.log(typeof(userList));
    return userList;
}

console.log(filterBy([90, null, undefined, "44", 46, "Rick", "Alex"], "undefined"));
