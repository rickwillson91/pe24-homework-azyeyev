// "use strict";
//
let error = document.createElement("p");

let input = document.getElementById("number");
let x = document.createElement("button");
x.textContent = "X"
let price = document.createElement("span")


input.addEventListener("focus", function () {
    input.classList.add("focus");
    if (input.classList.contains("invalid")) {
        input.classList.remove("invalid");
        error.textContent = "";

    }

});
input.addEventListener("blur", function () {
    if (input.value <= 0) {
        input.classList.add("invalid");
        error.textContent = "Please enter correct price"
        error.style.color = "red";
        input.after(error);
    }
    else {
        price.textContent = `Текущая цена: ${input.value}`;
        input.after(price);
        price.after(x);
        input.style.color = "green";
        x.addEventListener("click", function () {
            price.remove();
            x.remove();
            input.value = "";
        })

    }

})










