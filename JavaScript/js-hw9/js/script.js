"use strict";

let tabs = document.querySelector(".tabs");
let li = document.querySelectorAll(".tabs-title");
let liText = document.querySelectorAll(".tabs-item");

tabs.addEventListener("click", function (event) {
    li.forEach(item => {
        if (item.classList.contains("tabs-title-active")) {
            item.classList.remove("tabs-title-active")
        }

    })
    event.target.classList.add("tabs-title-active");
    liText.forEach(item => {
        if(item.classList.contains(event.target.dataset.name.toLowerCase())) {
            return item.classList.add("tabs-item-active");
        }
        item.classList.remove("tabs-item-active");

    })
})

