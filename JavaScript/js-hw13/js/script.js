let theme = document.getElementById("theme");
let header = document.querySelector("logo-box");
let grnBtn = document.querySelector("green");
let blBtn = document.querySelector("blue");

let page = document.querySelector(".page");
const spinner = document.querySelector(".spinner")

setTimeout(delayPage, 2000);

function delayPage(){
    spinner.style.cssText = "display : none"
    page.style.cssText = "display : block"
}


const darkmode = document.createElement("link")
darkmode.rel = "stylesheet"
darkmode.href = "./css/darkmode.css"


theme.addEventListener("click", changeTheme);
function changeTheme() {
    if (localStorage.getItem("Darkmode") !== null) {
        darkmode.remove();
        localStorage.removeItem("Darkmode")
    } else {
        document.head.append(darkmode);
        localStorage.setItem("Darkmode", "enabled");
    }
}
document.addEventListener("DOMContentLoaded", function () {
    if (localStorage.getItem("Darkmode") === "enabled") {
        document.head.append(darkmode);
    }
})
