const inputPass = document.getElementById("input-password");
const inputConf = document.getElementById("input-confirm");
const eye = document.querySelector(".fa-eye");
const eyeSlash = document.querySelector(".fa-eye-slash")
const btn = document.querySelector(".btn");
let error = document.createElement("p");
error.textContent = "Нужно ввести одинаковые значения";
error.style.color = "red"
eye.addEventListener("click", function (event) {
    if (eye.classList.contains("fa-eye")) {
        eye.classList.remove("fa-eye");
        eye.classList.add("fa-eye-slash");
        inputPass.type = "password";
    } else {
        eye.classList.remove("fa-eye-slash");
        eye.classList.add("fa-eye");
        inputPass.type = "text";
    }
})
eyeSlash.addEventListener("click", function (event) {
    if (eyeSlash.classList.contains("fa-eye-slash")) {
        eyeSlash.classList.remove("fa-eye-slash");
        eyeSlash.classList.add("fa-eye");
        inputConf.type = "text";
    } else {
        eyeSlash.classList.remove("fa-eye");
        eyeSlash.classList.add("fa-eye-slash");
        inputConf.type = "password";
    }
} )
btn.addEventListener("click", function (event) {
    if (inputPass.value === inputConf.value) {
        error.remove();
        alert("You are Welcome");
    } else {
        btn.before(error);
    }
    event.preventDefault();
});

