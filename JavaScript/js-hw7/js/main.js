"use strict";

function listCreator (list) {
    let newList = list.map(item => `<li>${item}</li>`);
    let newUl = document.createElement("ul");
    newList.forEach(item => {
        newUl.insertAdjacentHTML("beforeend", item);
    })
    document.body.append(newUl);
}

listCreator(['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv']);


