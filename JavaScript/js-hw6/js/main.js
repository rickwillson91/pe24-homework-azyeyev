"use strict";

function filterArray(list, type) {
    return list.filter(item => typeof(item) !== type);
}

console.log(filterArray([90, null, undefined, "44", 46, "Rick", "Alex"], "object"));

