const images = document.querySelectorAll(".image-to-show");
const stop = document.getElementById("stop");
const btnContinue = document.getElementById("continue");


let current = 0;

function slider() {
    // if (current + 1 === images.length) { // это первый вариант решения задачи - как зацыклить слайдер
    //     images[current].classList.remove("current-img")
    //     current = 0;
    //     images[current].classList.add("current-img");
    //     return;
    // }

    images[current].classList.remove("current-img")
    current = (current+1) % images.length; // продвинутое решение в одну строку
    images[current].classList.add("current-img"); // 3
}

let timer = setInterval(slider, 10000);
console.log(timer)

stop.addEventListener("click", (event) => {
    clearInterval(timer);
    console.log(timer)
})
btnContinue.addEventListener("click", (event)=> {
    timer = setInterval(slider, 10000);
    console.log(timer)
})










