"use strict"
function createSquares(quantity, size) {
    let userQuantity;
    do {
        userQuantity = +prompt("enter quantity of square?");
        if (userQuantity > 10) {
            alert("Error, quantity must be < 10")
        }
        continue;
    } while (isNaN(userQuantity) || userQuantity === 0);

    let squareArray = [];

    for (let i = 0; i < userQuantity; i++) {
        let userColor = prompt("choose color");
        let userSize = prompt("enter size of square in px");
        let square = {
            color: userColor,
            size: userSize,
        }
        squareArray.push(square)
    }
    squareArray.forEach((item, index) => {
        let newSquare = document.createElement("div");
        newSquare.style.cssText = `width : ${item.size}px; height : ${item.size}px; background-color : ${item.color}`;
        newSquare.className = `square-${index}`;
        document.body.append(newSquare);

    });



















}


createSquares();