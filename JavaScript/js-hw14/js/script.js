function backToTop() {
    let scrollBtn = $('.back-to-top');
    let screenheight = $(window).innerHeight();

    $(window).on('scroll', ()=>{
        console.log(this)
        if ($(this).scrollTop() > screenheight) {
            scrollBtn.fadeIn();
        } else {
            scrollBtn.fadeOut();
        }
    });
    scrollBtn.on('click', (evt) => {
        console.log("work")
        evt.preventDefault();
        $('HTML').animate({scrollTop:0}, 1500)
    })
}
backToTop();

$(".navigate-menu-item a").on("click", (event) => {
    event.preventDefault();
    let href = $(event.target).attr("href");
    let offset = $(href).offset().top;

    $("html, body").animate({
        scrollTop: offset
    },1000);

})



$(".navigation").on("click", () => {
    $(".navigate-menu").slideToggle();
});
$(".hide-news").on("click", () => {
    $(".hot-news-parent").slideToggle();
} )
