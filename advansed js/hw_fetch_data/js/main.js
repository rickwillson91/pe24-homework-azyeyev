
function renderStarWars() {
    const url = "https://swapi.dev/api/films/"
    const root = document.getElementById("root")

fetch(url)
    .then((response) => response.json())
    .then(({ results }) => {
        results.forEach(item => {
            let {episode_id, title, opening_crawl, characters} = item
            let wrapper = document.createElement("li");
            let name = document.createElement("h2");
            let openCrawl = document.createElement("p");
            let episode = document.createElement("p");
            let char = document.createElement("ol")
            char.textContent = "LIST OF ACTORS :"

            name.textContent = title;
            openCrawl.textContent = ` DESCRIPTION OF THAT EPISODE : ${opening_crawl}`;
            episode.textContent = `NUMBER OF EPISODE : ${episode_id}` ;

            wrapper.append(name, openCrawl, episode, char);
            root.append(wrapper);

            Promise.all(
                characters.map(urls =>
                    fetch(urls)
                        .then((res) => res.json())
                        .then(({name}) => {
                           characters.push(name);
                            let li = document.createElement("li")
                                li.textContent = name;
                            char.append(li);
                        })
                        .catch((error) => {
                            console.log(error.message)
                        })
                    )
                )
        })
    })
    .catch((error) => {
        console.log(error.message)
    })
}
renderStarWars();