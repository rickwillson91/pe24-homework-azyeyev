const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];


let BooksPusher = function (array) {
    let root = document.getElementById("root");
    let booklist = document.createElement("ul");
    root.append(booklist);

    array.forEach((item, index) => {
        try {
            if (!item.author) {
                throw new SyntaxError(`Book number ${index} does\'nt have the author`);
            } else if (!item.name) {
                throw new SyntaxError(`Book number ${index} does\'nt have the name`);
            } else if (!item.price) {
                throw new SyntaxError(`Book number ${index} does\'nt have the price`);
            } else {
                booklist.insertAdjacentHTML("beforeend",  `<li>${item.name}, ${item.author}, ${item.price},</li>`);
            }
        } catch (err) {
            console.log(err.message);
        }
    })
};
BooksPusher(books);



