class Employee {
    constructor(name, age, salary ) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    get nameInfo() {
        return this._name
    }
    get ageInfo() {
        return this._age
    }
    get salaryInfo() {
        return this._salary
    }
    set setName(newName) {
        this._name = newName;
    }
    set setAge(newAge) {
        this._age = newAge;
    }
    set setSalary(newSalary) {
        this._salary = newSalary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }
    get salaryInfo() {
       return this._salary * 3;
    }
}
const frontEnder = new Programmer("Rick", 29, 4000, ["javascript", "phyton",]);
const backEnder = new Programmer("Roy", 33, 4000, ["java", "phyton", "C#"]);

console.log(frontEnder);
console.log(backEnder);






















